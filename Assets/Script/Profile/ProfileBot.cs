﻿public static class ProfileBot
{
    private static int _win;

    private static int _lose;

    private static int _draws;

    private static int _move_last_round;

    static ProfileBot()
    {
        var ds = new DataService("existing.db");

        var people = ds.GetBot();

        foreach (Bot person in people)
        {
            _win = person.win;    

            _lose = person.lose;  

            _draws = person.draws;  

            _move_last_round = person.move_first_last_round;  
        }
    }

    public static int GetWin
    {
        get
        {
            return _win;
        }
        set
        {
            _win = value;
        }
    }

    public static int GetLose
    {
        get
        {
            return _lose;
        }
        set
        {
            _lose = value;
        }
    }

    public static int GetDraws
    {
        get
        {
            return _draws;
        }
        set
        {
            _draws = value;
        }
    }

    public static int GetMoveLastRound
    {
        get
        {
            return _move_last_round;
        }
        set
        {
            _move_last_round = value;
        }
    }

    public static void EditWin(int count)
    {
        GetWin = GetWin + count;

        var ds = new DataService("existing.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }

    public static void EditLose(int count)
    {
        GetLose = GetLose + count;

        var ds = new DataService("existing.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }

    public static void EditDraws(int count)
    {
        GetDraws = GetDraws + count;

        var ds = new DataService("existing.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }

    public static void EditMoveLastRound(int count)
    {
        GetMoveLastRound = count;

        var ds = new DataService("existing.db");

        ds.UpdateBot(GetWin,GetLose,GetDraws,GetMoveLastRound);
    }
	
}
