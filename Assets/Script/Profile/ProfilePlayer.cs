﻿public static class ProfilePlayer
{
    private static int _win;

    private static int _lose;

    private static int _draws;

    private static int _last_round;

    private static int _level;
 
    static ProfilePlayer()
    {
        var ds = new DataService("existing.db");

        var people = ds.GetPlayer();

        foreach (Player person in people)
        {
            _win = person.win;    

            _lose = person.lose;  

            _draws = person.draws;  

            _last_round = person.last_round;  
        }
    }

    public static int GetLevel
    {
        get
        {
            return _level;
        }
        set
        {
            _level = value;
        }
    }

    public static int GetWin
    {
        get
        {
            return _win;
        }
        set
        {
            _win = value;
        }
    }

    public static int GetLose
    {
        get
        {
            return _lose;
        }
        set
        {
            _lose = value;
        }
    }

    public static int GetDraws
    {
        get
        {
            return _draws;
        }
        set
        {
            _draws = value;
        }
    }

    public static int GetLastRound
    {
        get
        {
            return _last_round;
        }
        set
        {
            _last_round = value;
        }
    }

    public static void EditWin(int count)
    {
        GetWin = GetWin + count;

        var ds = new DataService("existing.db");

        ds.UpdatePlayer(GetWin,GetLose,GetDraws,GetLastRound);
    }

    public static void EditLose(int count)
    {
        GetLose = GetLose + count;

        var ds = new DataService("existing.db");

        ds.UpdatePlayer(GetWin,GetLose,GetDraws,GetLastRound);
    }

    public static void EditDraws(int count)
    {
        GetDraws = GetDraws + count;

        var ds = new DataService("existing.db");

        ds.UpdatePlayer(GetWin,GetLose,GetDraws,GetLastRound);
    }

    public static void EditLastRound(int count)
    {
        GetLastRound = count;

        var ds = new DataService("existing.db");

        ds.UpdatePlayer(GetWin,GetLose,GetDraws,GetLastRound);
    }
}