﻿using UnityEngine;

public class GameManager : MonoBehaviour
{	
    private static GameManager m_instance;

    public static GameManager Instance { get { return m_instance; } set {;} }

    void Awake()
    {
        m_instance = this;
    }

    void Start ()
	{		
		LoadWindowManager.LoadAllWindowInGame (); 
	}
    void Update()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        Screen.autorotateToPortrait = false;

        Screen.autorotateToPortraitUpsideDown = false;
    }
}
