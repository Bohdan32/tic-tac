﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CellInBoard : MonoBehaviour , IPointerDownHandler
{
    [SerializeField]
    private SpriteRenderer _cellSpriteRender;

    [SerializeField]
    private Sprite _spriteZero;

    [SerializeField]
    private Sprite _spriteCross;

    [SerializeField]
    private Sprite _spriteNormal;

    [SerializeField]
    private eState _wasClicked;

    private int _сellValue = -1;

    public int GetCellValue
    {
        get
        {
            return _сellValue;
        }
    }

    public bool GoPlayer; 

    private int x;

    private int y;
	
    public int PositionX
    {
        get
        {       
            return x;
        }
        set
        { 
            x = value;
        }
    }

    public int PositionY
    {
        get
        {       
            return y;
        }
        set
        { 
            y = value;
        }
    }

    public BoardController BoardController{ get; set;}

    public void OnPointerDown(PointerEventData eventData)
    {
       
        if (_wasClicked == eState.NoClick)// check repeated click
        {
            _wasClicked = eState.Click;

            eTarget target = (eTarget)BoardController.Instance.GoRun(PositionX, PositionY);

            switch (target)
            {
                case eTarget.cross:
                    _cellSpriteRender.sprite = _spriteCross;
                    break;

                case eTarget.zerro:
                    _cellSpriteRender.sprite = _spriteZero;
                    break;
            }   
        }
        else
        {
            Debug.Log("Повторный клик!!!!!!!!!!!!!!!");
        }
    }  
    public enum eState
    {
        NoClick,
        Click
    }
}
