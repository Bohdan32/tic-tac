﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class BoardController : MonoBehaviour 
{
    public static BoardController Instance;

    private CellInBoard [,] _cellInBoard = new CellInBoard[3,3];

    [SerializeField]
    private CellInBoard _cellBase;

    private eTarget[,] _cellValue = new eTarget[3, 3]; 

    private float WidthCell;

    private float PaddindCell;

    private Player p;

    private eTarget BotTarget;

    private List<Vector2>  emptyCell = new List<Vector2>();

    private int _botLevel;  

    public int GetLevel
    {
        get
        { 
            return _botLevel;
        }
        set
        { 
            _botLevel = value;
        }
    }

    void Awake()
    {
        Instance = this;

    }

    private void OnEnable()
    {
        _botLevel = ProfilePlayer.GetLevel; // this value check level bot;

        WidthCell = _cellBase.transform.GetComponent<SpriteRenderer>().bounds.size.x;

        PaddindCell = WidthCell / 4;

        InstantiateCellInBoard(); // Building field 

        CheckWhoMove(ProfilePlayer.GetLastRound);   // Check who run first(Bot or Player);
   
    }
 
    private void InstantiateCellInBoard()
    {  
        for (int i = 0; i < 3; i++)
        {

            for (int k = 0; k < 3; k++)
            {

            GameObject cell = Instantiate(_cellBase.gameObject, _cellBase.gameObject.transform.parent, false) as GameObject;

            cell.transform.parent = gameObject.transform;

            cell.transform.position = _cellBase.transform.position;

                cell.GetComponent<Transform>().position = (new Vector2(i * (WidthCell + PaddindCell) + _cellBase.GetComponent<Transform>().position.x,
                    -k * (WidthCell + PaddindCell) + _cellBase.GetComponent<Transform>().position.y));
            
                cell.name = k + ":" + i;

                CellInBoard viewCellInBoard = cell.GetComponent<CellInBoard>();

                viewCellInBoard.PositionX = k;

                viewCellInBoard.PositionY = i;

                viewCellInBoard.BoardController = this;

                _cellInBoard[i, k] = viewCellInBoard;

                cell.SetActive(true);  

            }
        }

    }


    public int CheckLine()  // Functions check wins. If function returns 2 - bot wins, if 1 - player wins, in 0 - game continue;
    {
        int b = 0;

       for(int i = 0; i< 3; i++)
        {
            if (_cellValue[i, 0] == _cellValue[i, 1]  && _cellValue[i, 1]== _cellValue[i, 2] && _cellValue[i, 2]!= eTarget.empty)
            {
                
                if(_cellValue[i, 2] == BotTarget)
                {
                    b = 2;
                    break;
                }
                else
                {
                    b = 1;  
                }
            }

            if (_cellValue[0, i] == _cellValue[1, i] && _cellValue[1, i] == _cellValue[2, i] && _cellValue[0, i]!= eTarget.empty)
            {
                if(_cellValue[0, i] == BotTarget)
                {
                    b = 2;
                    break;
                }
                else
                {
                    b = 1;  
                }
            }

        }
        if (_cellValue[0, 0] == _cellValue[1, 1]&& _cellValue[1, 1] == _cellValue[2, 2]&& _cellValue[0, 0]!= eTarget.empty)
        {
            if(_cellValue[0, 0] == BotTarget)
            {
                b = 2;
            }
            else
            {
                b = 1;  
            }
        }
        if (_cellValue[0, 2] == _cellValue[1, 1]&& _cellValue[1, 1] == _cellValue[2, 0]&& _cellValue[2, 0]!= eTarget.empty)
        {
            if (_cellValue[2, 0] == BotTarget)
            {
                b = 2;  
            }
            else
            {
                b = 1;  
            }
        }

        return b;
    }


    public void CheckWhoMove(int a) // Functions check who run first(Bot or Player) and . If bot - do first move
    {
        switch (a)
        {
            case 1:
                p = new Player(new RunPlayer(), new Cross()); 
                BotTarget = eTarget.zerro;
                ProfileBot.EditMoveLastRound(2);
                break;

            case 2:
                p = new Player(new RunBot(), new Cross());
                BotTarget = eTarget.cross;
                ProfileBot.EditMoveLastRound(1);
                switch (_botLevel)
                {
                    case 1:
                        BotEasyLevel(BotTarget);
                        break;
                    case 2:
                        BotMediumLevel(BotTarget);
                        break;
                    case 3:
                        BotHardLevel(BotTarget);
                        break;
                }
                break;

            case 0:
                
                if (ProfileBot.GetMoveLastRound == 2)
                {
                    p = new Player(new RunBot(), new Cross());
                    BotTarget = eTarget.cross;
                    ProfileBot.EditMoveLastRound(1);
                    switch (_botLevel)
                    {
                        case 1:
                            BotEasyLevel(BotTarget);
                            break;
                        case 2:
                            BotMediumLevel(BotTarget);
                            break;
                        case 3:
                            BotHardLevel(BotTarget);
                            break;
                    }
                }
                else
                {
                    if (ProfileBot.GetMoveLastRound ==1)
                    {
                        p = new Player(new RunPlayer(), new Cross());
                        ProfileBot.EditMoveLastRound(2);
                        BotTarget = eTarget.zerro;
                    }   
                }
                break;
        }  
    }




    //The first level bot is doing random moves to free cells
    public void BotEasyLevel(eTarget target) // If player chose level "easy"
    {  
        emptyCell.Clear();

        for( int i = 0; i < 3; i++)
        {
            for( int j = 0; j < 3; j++)
            {
                if (_cellValue[i , j] == eTarget.empty)
                {
                    emptyCell.Add(new Vector2( i , j));// Add ihere all available position, that bot knows where to do
                }
            }
        }    
        if (emptyCell.Count == 0)// Check the draw
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else  
        {
            int r = (int)UnityEngine.Random.Range(0 , emptyCell.Count-1);

            if (_cellValue[(int)emptyCell[r].x, (int)emptyCell[r].y] == eTarget.empty)
            {
                _cellValue[(int)emptyCell[r].x, (int)emptyCell[r].y] = target;

                _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].OnPointerDown(null);  
            }
            else  //Check if position is incorrectly defined
            {
                switch (_botLevel)
                {
                    case 1:
                        BotEasyLevel(BotTarget);
                        break;
                    case 2:
                        BotMediumLevel(BotTarget);
                        break;
                    case 3:
                        BotHardLevel(BotTarget);
                        break;
                }
            }
        }
    }

    //The medium level bot do analiz of user moves. Bot closes the last cell of the user else do random move;
    public void BotMediumLevel(eTarget target)
    { 
        emptyCell.Clear();

        for( int i = 0; i < 3; i++)
        {
            for( int j = 0; j < 3; j++)
            {
                if (_cellValue[i , j] == eTarget.empty)
                {
                    emptyCell.Add(new Vector2( i , j));
                }
            }
        }    
        if (emptyCell.Count == 0)
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else
        {

            if (AnalizRunPlayerMediumLevel(target) == true)
            {

            }
            else
            {
                int r = (int)UnityEngine.Random.Range(0 , emptyCell.Count-1);

                if (_cellValue[(int)emptyCell[r].x, (int)emptyCell[r].y] == eTarget.empty)
                {
                    _cellValue[(int)emptyCell[r].x, (int)emptyCell[r].y] = target;

                    _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].OnPointerDown(null);  
                }
                else
                {
                    switch (_botLevel)
                    {
                        case 1:
                            BotEasyLevel(BotTarget);
                            break;
                        case 2:
                            BotMediumLevel(BotTarget);
                            break;
                        case 3:
                            BotHardLevel(BotTarget);
                            break;
                    }
                }
            }

            //After the move bot we need to check the free cells,this may be the last move and maybe is draw. 
            emptyCell.Clear();

            for( int i = 0; i < 3; i++)
            {
                for( int j = 0; j < 3; j++)
                {
                    if (_cellValue[i , j] == eTarget.empty)
                    {
                        emptyCell.Add(new Vector2( i , j));
                    }
                }
            }     
            if (emptyCell.Count == 0)
            {
                ProfilePlayer.EditLastRound(0);

                UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

                UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
            }
        } 
    }

    //Field analysis medium level
    private bool AnalizRunPlayerMediumLevel(eTarget target)
    {
        bool result = false;

        for(int i = 0; i< 3; i++)
        {
            if (_cellValue[i, 0] == _cellValue[i, 1] && _cellValue[i, 0]!= eTarget.empty) //Analiz all vertical line 1 and 2 cells
            { 
                if (_cellValue[i, 2] == eTarget.empty)
                {    
                    result = true;

                    _cellValue[i, 2] = target;   

                    _cellInBoard[i, 2].OnPointerDown(null);

                    break;
                }
            } 
             
            if (_cellValue[i, 1] == _cellValue[i, 2] && _cellValue[i, 2]!= eTarget.empty) //Analiz all vertical line 2 and 3 cells
            {    
                if (_cellValue[i, 0] == eTarget.empty)
                {  
                    result = true;

                    _cellValue[i, 0] = target;   

                    _cellInBoard[i, 0].OnPointerDown(null);
                    break;
                }
            }
             
            if (_cellValue[0, i] == _cellValue[1, i] && _cellValue[0, i]!= eTarget.empty)//Analiz all horizontal line 1 and 2 cells
            {
                if (_cellValue[2, i] == eTarget.empty)
                { 
                    result = true;

                    _cellValue[2, i] = target;   

                    _cellInBoard[2, i].OnPointerDown(null);

                    break;
                }         
            }

            if (_cellValue[1, i] == _cellValue[2, i] && _cellValue[1, i]!= eTarget.empty)//Analiz all horizontal line 2 and 3 cells
            {
                if (_cellValue[0, i] == eTarget.empty)
                {   
                    result = true;

                    _cellValue[0, i] = target;   

                    _cellInBoard[0, i].OnPointerDown(null);

                    break;
                }      
            }
            //Diagonals
            if (_cellValue[0, 0] == _cellValue[1, 1]&& _cellValue[0, 0]!= eTarget.empty)//Down from right to left 1 and 2 elements
            {
                if (_cellValue[2, 2] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, 2] = target;   

                    _cellInBoard[2, 2].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[1, 1] == _cellValue[2, 2]&&_cellValue[1, 1]!= eTarget.empty)//Down from right to left 2 and 3 elements
            {
                if (_cellValue[0, 0] == eTarget.empty)
                {
                    result = true;

                    _cellValue[0, 0] = target;   

                    _cellInBoard[0, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[0, 2] == _cellValue[1, 1]&& _cellValue[0, 2]!= eTarget.empty)//Up from left to right 1 and 2 elements 
            {
                if (_cellValue[2, 0] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, 0] = target;   

                    _cellInBoard[2, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[1, 1] == _cellValue[2, 0]&&_cellValue[1, 1]!= eTarget.empty)//Up from left to right 2 and 3 elements 
            {
                if (_cellValue[0, 2] == eTarget.empty)
                {
                    result = true;

                    _cellValue[0, 2] = target;   

                    _cellInBoard[0, 2].OnPointerDown(null);

                    break;
                }
            }
        }
        return result;
    }

    //The hard level bot first do analiz of him cell. If he now do and can win, he does it. 
    //If previous analiz false, he do analiz of user moves. Bot closes the last cell of the user else do random move;
    //If this analiz false, he do random moves;
    public void BotHardLevel(eTarget target)
    { 
        emptyCell.Clear();

        for( int i = 0; i < 3; i++)
        {
            for( int j = 0; j < 3; j++)
            {
                if (_cellValue[i , j] == eTarget.empty)
                {
                    emptyCell.Add(new Vector2( i , j));
                }
            }
        }    

        if (emptyCell.Count == 0)
        {
            ProfilePlayer.EditLastRound(0);

            UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

            UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
        }
        else
        {
            if (AnalizRunBotHardLevel(target) == true)
            {}
            else
            {
                if (AnalizRunPlayerHardLevel(target) == true)
                {}
                else
                {
                    int r = (int)UnityEngine.Random.Range(0 , emptyCell.Count-1);

                    if (_cellValue[(int)emptyCell[r].x, (int)emptyCell[r].y] == eTarget.empty)
                    {
                        _cellValue[(int)emptyCell[r].x, (int)emptyCell[r].y] = target;

                        _cellInBoard[(int)emptyCell[r].x, (int)emptyCell[r].y].OnPointerDown(null);  
                    }
                    else
                    {
                        switch (_botLevel)
                        {
                            case 1:
                                BotEasyLevel(BotTarget);
                                break;
                            case 2:
                                BotMediumLevel(BotTarget);
                                break;
                            case 3:
                                BotHardLevel(BotTarget);
                                break;
                        }
                    }
                }
                emptyCell.Clear();

                for( int i = 0; i < 3; i++)
                {
                    for( int j = 0; j < 3; j++)
                    {
                        if (_cellValue[i , j] == eTarget.empty)
                        {
                            emptyCell.Add(new Vector2( i , j));
                        }
                    }
                }     
                if (emptyCell.Count == 0)
                {
                    ProfilePlayer.EditLastRound(0);

                    UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(0);

                    UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);
                }
            } 
            }
    }


    //Analiz of bot cell
    private bool AnalizRunBotHardLevel(eTarget target)
    {
        bool result = false;

        for(int i = 0; i< 3; i++)
        {
            if (_cellValue[i, 0] == _cellValue[i, 1] && _cellValue[i, 0]==BotTarget) //Vertical 1 and 2
            {

                if (_cellValue[i, 2] == eTarget.empty)
                {    
                    result = true;

                    _cellValue[i, 2] = target;   

                    _cellInBoard[i, 2].OnPointerDown(null);

                    break;
                }
            }  

            if (_cellValue[i, 1] == _cellValue[i, 2] && _cellValue[i, 2]==BotTarget) //Vertical 2 and 3
            {    
                if (_cellValue[i, 0] == eTarget.empty)
                {  
                    result = true;

                    _cellValue[i, 0] = target;   

                    _cellInBoard[i, 0].OnPointerDown(null);

                    break;
                }
            } 

            if (_cellValue[0, i] == _cellValue[1, i] && _cellValue[0, i]==BotTarget)//Horizontal 1 and 2
            {
                if (_cellValue[2, i] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, i] = target;   

                    _cellInBoard[2, i].OnPointerDown(null);

                    break;
                }         
            }

            if (_cellValue[1, i] == _cellValue[2, i] && _cellValue[1, i]==BotTarget)//Horizontal 2 and 3
            {
                if (_cellValue[0, i] == eTarget.empty)
                {   
                    result = true;

                    _cellValue[0, i] = target;   

                    _cellInBoard[0, i].OnPointerDown(null);

                    break;
                }      
            }

            if (_cellValue[0, 0] == _cellValue[1, 1]&& _cellValue[0, 0]==BotTarget)//Вниз справа налево 1 и 2 элементы  
            {
                if (_cellValue[2, 2] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, 2] = target;   

                    _cellInBoard[2, 2].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[1, 1] == _cellValue[2, 2]&&_cellValue[1, 1]==BotTarget)//Вниз справа налево 2 и 3 элементы  
            {
                if (_cellValue[0, 0] == eTarget.empty)
                {
                    result = true;

                    _cellValue[0, 0] = target;   

                    _cellInBoard[0, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[0, 2] == _cellValue[1, 1]&& _cellValue[0, 2]==BotTarget)//Вверх слева направо 1 и 2 элементы  
            {
                if (_cellValue[2, 0] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, 0] = target;   

                    _cellInBoard[2, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[1, 1] == _cellValue[2, 0]&&_cellValue[1, 1]==BotTarget)//Вверх слева направо 2 и 3 элементы  
            {
                if (_cellValue[0, 2] == eTarget.empty)
                {
                    result = true;

                    _cellValue[0, 2] = target;   

                    _cellInBoard[0, 2].OnPointerDown(null);

                    break;
                }
            }
        }
        return result;
    }

    //Analiz of player cell
    private bool AnalizRunPlayerHardLevel(eTarget target)
    {
        bool result = false;

        for(int i = 0; i< 3; i++)
        {
            if (_cellValue[i, 0] == _cellValue[i, 1] && _cellValue[i, 0]!=BotTarget&&_cellValue[i, 0]!=eTarget.empty) //Vertical 1 and 2
            {
                if (_cellValue[i, 2] == eTarget.empty)
                {    
                    result = true;

                    _cellValue[i, 2] = target;   

                    _cellInBoard[i, 2].OnPointerDown(null);

                    break;
                }
            }  

            if (_cellValue[i, 1] == _cellValue[i, 2] && _cellValue[i, 2]!=BotTarget&&_cellValue[i, 2]!=eTarget.empty) //Vertical 2 and 3
            {    
                if (_cellValue[i, 0] == eTarget.empty)
                {  
                    result = true;

                    _cellValue[i, 0] = target;   

                    _cellInBoard[i, 0].OnPointerDown(null);

                    break;
                }
            } 

            if (_cellValue[0, i] == _cellValue[1, i] && _cellValue[0, i]!=BotTarget&&_cellValue[0, i]!=eTarget.empty)//Horizontal 1 and 2
            {
                if (_cellValue[2, i] == eTarget.empty)
                {   
                    result = true;

                    _cellValue[2, i] = target;   

                    _cellInBoard[2, i].OnPointerDown(null);

                    break;
                }         
            }

            if (_cellValue[1, i] == _cellValue[2, i] && _cellValue[1, i]!=BotTarget&&_cellValue[1, i]!=eTarget.empty)//Horizontal 2 and 3
            {
                if (_cellValue[0, i] == eTarget.empty)
                {   
                    result = true;

                    _cellValue[0, i] = target;   

                    _cellInBoard[0, i].OnPointerDown(null);

                    break;
                }      
            }

            if (_cellValue[0, 0] == _cellValue[1, 1]&& _cellValue[0, 0]!=BotTarget&&_cellValue[0, 0]!=eTarget.empty)//Вниз справа налево 1 и 2 элементы  
            {
                if (_cellValue[2, 2] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, 2] = target;   

                    _cellInBoard[2, 2].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[1, 1] == _cellValue[2, 2]&&_cellValue[1, 1]!=BotTarget&&_cellValue[1, 1]!=eTarget.empty)//Вниз справа налево 2 и 3 элементы  
            {
                if (_cellValue[0, 0] == eTarget.empty)
                {
                    result = true;

                    _cellValue[0, 0] = target;   

                    _cellInBoard[0, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[0, 2] == _cellValue[1, 1]&& _cellValue[0, 2]!=BotTarget&&_cellValue[0, 2]!=eTarget.empty)//Вверх слева направо 1 и 2 элементы  
            {
                if (_cellValue[2, 0] == eTarget.empty)
                {
                    result = true;

                    _cellValue[2, 0] = target;   

                    _cellInBoard[2, 0].OnPointerDown(null);

                    break;
                }
            }

            if (_cellValue[1, 1] == _cellValue[2, 0]&&_cellValue[1, 1]!=BotTarget&&_cellValue[1, 1]!=eTarget.empty)//Вверх слева направо 2 и 3 элементы  
            {
                if (_cellValue[0, 2] == eTarget.empty)
                {
                    result = true;

                    _cellValue[0, 2] = target;   

                    _cellInBoard[0, 2].OnPointerDown(null);

                    break;
                }
            }
        }
        return result;
    }

    public Enum GoRun(int pos_x , int pos_y)// Function moves player and bot
    {
        eTarget target = (eTarget)p.Run();

        switch (target)
        {
            case eTarget.cross:

                 _cellValue[pos_y, pos_x] = eTarget.cross;// write result moves 

                if (CheckLine()==1)// Wins Player
                {
                    ProfilePlayer.EditLastRound(1);

                    UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(1);

                    UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

                    break;
                }
                if (CheckLine()==2)// Wins Bot
                {
                    ProfilePlayer.EditLastRound(2);

                    UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(2);

                    UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

                    break;
                }
                else
                {
                    if(BotTarget != eTarget.cross)// if this move no bot we need start move bot
                    {
                        switch (_botLevel)
                        {
                            case 1:
                                BotEasyLevel(BotTarget);
                                break;
                            case 2:
                                BotMediumLevel(BotTarget);
                                break;
                            case 3:
                                BotHardLevel(BotTarget);
                                break;
                        }
                    } 
                }
                break;

            case eTarget.zerro:

               _cellValue[pos_y, pos_x] = eTarget.zerro;

                if (CheckLine()==1)
                {
                    ProfilePlayer.EditLastRound(1);

                    UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(1);

                    UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

                    break;
                }
                if (CheckLine()==2)
                {
                    ProfilePlayer.EditLastRound(2);

                    UnityPoolManager.Instance.GetBaseWindowObject(LoadWindowManager.eWindowNameInLocation.ResultWindow).GetComponent<ViewResult>().Construct(2);

                    UnityPoolManager.Instance.Pop(LoadWindowManager.eWindowNameInLocation.ResultWindow);

                    break;
                }
                else
                {
                    if(BotTarget != eTarget.zerro)// if this move no bot we need start move bot
                    {
                        switch (_botLevel)
                        {
                            case 1:
                                BotEasyLevel(BotTarget);
                                break;
                            case 2:
                                BotMediumLevel(BotTarget);
                                break;
                            case 3:
                                BotHardLevel(BotTarget);
                                break;
                        }
                    }
                }
                break;
        }
        return target;
    }


    //Next 2 state change state player and bot: if one of them move croos, other automatically receives zerro. Also we change of moves. 

    // State - 2 
    interface IRun
    {
        eTarget Run(Run run);
    }

    class Zerro : IRun
    {
        public eTarget Run(Run run)
        {
            run.State = new Cross();
            return eTarget.zerro;
        }
    }

    class Cross : IRun
    {
        public eTarget Run(Run run)
        {
            run.State = new Zerro();
            return eTarget.cross;
        }  
    }

    class Run
    {
        public IRun State { get; set; }

        public Run(IRun run)
        {
            State = run;
        }

        public eTarget ValueRun()
        {
          return State.Run(this); 
        }
    }

    // State - 1 
    class Player
    {
        public IPlayer State{get;set;}

        public Run run;

        public Player(IPlayer player , IRun stateRun)
        {
            State = player;

            run = new Run(stateRun);
        }

        public Enum Run()
        {
            State.Run(this); 

            return run.ValueRun();
        }
    }

    interface IPlayer
    {
        void Run(Player player );
    }

    class RunPlayer : IPlayer
    {
        public void Run(Player player)
        {
            player.State = new RunBot(); 
        }
    }

    class RunBot : IPlayer
    {
        public void Run(Player player)
        {
            player.State = new RunPlayer();
        }
    }
}

public enum eTarget
{
    empty,
    cross,
    zerro,
}















