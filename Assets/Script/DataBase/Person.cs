﻿using SQLite4Unity3d;

public class Player  {

	[PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int win { get; set; }
    public int lose { get; set; }
    public int draws { get; set; }
    public int last_round { get; set; }
}
